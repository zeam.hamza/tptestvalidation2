using Xunit;
using Business;
using System;

namespace Test
{
    public class LoanTest
    {
        private const int YEARS_TO_MONTHS = 12;
        private static readonly Capital CAPITAL_AMOUNT = 175000;
        private static readonly LoanDuration LOAN_DURATION = 25 * YEARS_TO_MONTHS;
        private static readonly DefferedDuration DEFFERED_DURATION = 1;
        private static readonly Insurance INSURANCE = 0.7m;
        private static readonly Interest INTEREST = 1.27m;

        private static readonly Loan LOAN = new Loan(CAPITAL_AMOUNT, INSURANCE, INTEREST, LOAN_DURATION, DEFFERED_DURATION);

        [Fact]
        public void CapitalCorrectTest()
        {
            Capital BorrowedCapital = 175000;
            Assert.Equal<Capital>(175000, BorrowedCapital);
        }

        [Fact]
        public void InsuranceCorrectTest()
        {
            Insurance insurance = 0.7m;
            Assert.Equal<Insurance>(0.7m, insurance);
        }

        [Fact]
        public void InterestCorrectTest()
        {
            Interest interest = 1.27m;
            Assert.Equal<Interest>(1.27m, interest);
        }

        [Fact]
        public void LoanDurationCorrectTest()
        {
            LoanDuration loanduration = 25 * YEARS_TO_MONTHS;
            Assert.Equal<LoanDuration>(25 * YEARS_TO_MONTHS, loanduration);
        }

        [Fact]
        public void LoanDurationIncorrectTest()
        {
            LoanDuration loanduration;
            Assert.Throws<ArgumentOutOfRangeException>(() => loanduration = LoanDuration.MIN_DURATION - 1);
            Assert.Throws<ArgumentOutOfRangeException>(() => loanduration = LoanDuration.MAX_DURATION + 1);
        }

        [Fact]
        public void TestCorrectDeffered()
        {
            Loan testLoan = new Loan(CAPITAL_AMOUNT, INSURANCE, INTEREST, LOAN_DURATION, LOAN_DURATION - 1);
        }

        [Fact]
        public void TestIncorrectDeffered()
        {
            Loan testLoan;
            Assert.Throws<ArgumentOutOfRangeException>(() => 
                testLoan = new Loan(CAPITAL_AMOUNT, INSURANCE, INTEREST, LOAN_DURATION, LOAN_DURATION + 1
                ));
        }

        [Fact]
        public void TestCalculationMonthlyPayment()
        {
            Currency monthlyPayment = LOAN.MonthlyPayment();
            Assert.Equal<Currency>(783, Math.Round(monthlyPayment));
        }
        [Fact]
        public void TestCalculationMonthlyInsurancePayment()
        {
            Currency monthlyInsurance = LOAN.MonthlyInsurancePayment();
            Assert.Equal<Currency>(102, monthlyInsurance);
        }

        [Fact]
        public void TestCalculationTotalInsurancePayment()
        {
            Currency totalInsurance = LOAN.TotalInsurancePayment();
            Assert.Equal<Currency>(30624, totalInsurance);
        }

        [Fact]
        public void TestCalculationTotalInterestPayment()
        {
            Currency totalInterest = LOAN.TotalInterestPayment();
            Assert.Equal<Currency>(59964, totalInterest);
        }
    }
}