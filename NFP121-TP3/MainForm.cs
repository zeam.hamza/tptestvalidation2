using Business;

namespace NFP121
{
    public partial class MainForm : Form
    {

        private Insurance insurance = 0m;
        //private NominalRate nominalRate = 0m;
        private LoanDuration duration = LoanDuration.MIN_DURATION;
        private DefferedDuration defferedDuration = 0;
        private Interest interest = 0;
        private Capital capital = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            capital = (Capital)nudCapital.Value;
            insurance = (Insurance)nudInsurance.Value;
            duration = (LoanDuration)nudDuration.Value;
            defferedDuration = (DefferedDuration)nudDefferedDuration.Value;
            interest = (Interest)nudInterest.Value;

            //UpdateNominalRate();
            UpdateCalculatedValues();
        }

        private void nudDuration_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                duration = (LoanDuration)nudDuration.Value;
                UpdateCalculatedValues();
            }
            catch (ArgumentOutOfRangeException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private void nudDefferedDuration_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                defferedDuration = (DefferedDuration)nudDefferedDuration.Value;
            }
            catch (ArgumentOutOfRangeException exception)
            {
                Console.WriteLine(exception.Message);
            }

            try
            {
                UpdateCalculatedValues();
            }
            catch (ArgumentOutOfRangeException exception)
            {
                Console.WriteLine(exception.Message);
                nudDefferedDuration.Value = duration - 1;
                nudDefferedDuration_ValueChanged(sender, e);
            }
        }

        private void nudCapital_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                capital = (Capital)nudCapital.Value;
                UpdateCalculatedValues();
            }
            catch (ArgumentOutOfRangeException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private void nudInsurance_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                insurance = (Insurance)nudInsurance.Value;
                UpdateCalculatedValues();
            }
            catch (ArgumentOutOfRangeException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private void nudInterest_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                interest = (Interest)nudInterest.Value;
                UpdateCalculatedValues();
            }
            catch (ArgumentOutOfRangeException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private void UpdateCalculatedValues()
        {
            var calculator = new Loan(
                capital, insurance, interest, duration, defferedDuration);

            lblQ1.Text = Math.Round((decimal)calculator.MonthlyPayment()).ToString();
            lblQ2.Text = Math.Round((decimal)calculator.MonthlyInsurancePayment()).ToString();
            lblQ3.Text = Math.Round((decimal)calculator.TotalInterestPayment()).ToString();
            lblQ4.Text = Math.Round((decimal)calculator.TotalInsurancePayment()).ToString();
            lblQ5.Text = Math.Round((decimal)calculator.PaymentInTenYears()).ToString();
        }
    }
}