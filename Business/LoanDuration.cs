﻿namespace Business
{
    public class LoanDuration
    {
        private const uint YEARS_TO_MONTHS = 12;
        public const uint MIN_DURATION = 9 * YEARS_TO_MONTHS;
        public const uint MAX_DURATION = 25 * YEARS_TO_MONTHS;

        public LoanDuration(uint months)
        {
            if (months < MIN_DURATION || months > MAX_DURATION)
            {
                throw new ArgumentOutOfRangeException(
                    String.Format("The loan must span between {0} and {1} months.", MIN_DURATION, MAX_DURATION));
            }

            this.Value = months;
        }

        override public string ToString()
        {
            return Value.ToString();
        }

        public override bool Equals(object? obj)
        {
            if (obj != null)
            {
                return (obj as LoanDuration).Value == Value;
            }
            else
            {
                return base.Equals(obj);
            }
        }

        public static implicit operator LoanDuration(uint value) => new(value);
        public static implicit operator uint(LoanDuration value) => value.Value;

        uint Value { get; set; }

        public uint ToYears()
        {
            return this.Value / YEARS_TO_MONTHS;
        }
    }
}