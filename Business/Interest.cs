﻿namespace Business
{
    public class Interest
    {
        public Interest(decimal value)
        {
            this.Value = value;
        }

        override public string ToString()
        {
            return Value.ToString();
        }

        public override bool Equals(object? obj)
        {
            if (obj != null)
            {
                return (obj as Interest).Value == Value;
            }
            else
            {
                return base.Equals(obj);
            }
        }

        public static implicit operator Interest(decimal value) => new(value);
        public static implicit operator decimal(Interest value) => value.Value;

        decimal Value { get; }
    }
}