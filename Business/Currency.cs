﻿namespace Business
{
    public class Currency
    {
        public Currency(decimal value)
        {
            this.Value = value;
        }

        override public string ToString()
        {
            return Value.ToString();
        }

        public override bool Equals(object? obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }
            Currency other = (Currency)obj;

            return this.Value == other.Value;
        }

        public static implicit operator Currency(decimal value) => new(value);
        public static implicit operator Currency(Insurance value) => new(Decimal.ToUInt32(value));
        public static implicit operator decimal(Currency value) => value.Value;

        decimal Value { get; }
    }
}