﻿namespace Business
{
    public class Capital
    {
        public Capital(uint value)
        {
            this.Value = value;
        }

        override public string ToString()
        {
            return Value.ToString();
        }

        public override bool Equals(object? obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }
            Capital other = (Capital)obj;

            return this.Value == other.Value;
        }

        public static implicit operator Capital(uint value) => new(value);
        public static implicit operator uint(Capital value) => value.Value;

        uint Value { get; }
    }
}